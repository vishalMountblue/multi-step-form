let nextButton = document.querySelector("#next-button");
let backButton = document.querySelector("#back-button");
let navigator = document.querySelector(".navigator");
let page = 1;

function forwardActions() {
  switch (page) {
    case 1:
      validateForm();
      break;
    case 2:
      validatePlan();
      break;
    case 3:
      validateAddOns();
      break;

    case 4:
      displayThankYouPage();
      break;
  }
}

nextButton.addEventListener("click", forwardActions);

function validateForm() {
  page++;
  let allInputFieldErrorClass = document.querySelectorAll(".input-field-error");
  let inputContainers = document.querySelectorAll(".input-container");

  let inputTag = 1;
  let inputContainer;
  let inputValue;
  let validated = true;
  let validatedCount = 0;

  allInputFieldErrorClass.forEach((fieldError, index) => {
    inputContainer = inputContainers[index];
    inputValue = inputContainer.children[inputTag].value;

    if (inputValue === "") {
      fieldError.style.visibility = "visible";
      validated = false;
    } else {
      fieldError.style.visibility = "hidden";
      validatedCount++;
    }
  });

  if (validatedCount === 3) validated = true;
  else page = 1;

  if (validated) {
    hideFirstPage();
    showSecondPage();
  }
}

function hideFirstPage() {
  let personalContainer = document.getElementById("personal-details");
  personalContainer.classList.add("display-none");
}

function showSecondPage() {
  let heading = "Select Your Plan";
  let subHeading = "You have the option of monthly or yearly billing";
  changeHeadingContent(heading, subHeading);

  let plan = document.getElementById("plan-details");
  plan.classList.remove("display-none");

  showBackButton();

  let firstNav = document.getElementById("first-nav");
  resetNavigationBackground(firstNav);

  let secondNav = document.getElementById("second-nav");
  setNavigationBackground(secondNav);
}

// plan page validation
let toggleCount = 1;
let planType = "monthly";

function validatePlan() {
  page++;
  if (toggleCount % 2 === 0) {
    planType = "yearly";
  } else {
    planType = "monthly";
  }

  hidePlanPage();
  showAddOnPage(planType);
}

function hidePlanPage() {
  let plan = document.getElementById("plan-details");
  plan.classList.add("display-none");
}

// plan page functions

let yearlyPlan = 0;
let planTextIndex = 1;
let planNameIndex = 0;
let moneyIndex = 1;

let defaultPlanSelected;
let defaultPlanName;
let defaultMoney;

let plan;
let planName;
let money;

function setDefaultPlanAndPrice() {
  defaultPlanSelected = document.querySelector(".plan");
  defaultPlanName =
    defaultPlanSelected.children[planTextIndex].children[planNameIndex]
      .textContent;
  defaultMoney =
    defaultPlanSelected.children[planTextIndex].children[moneyIndex]
      .textContent;

  setPlanBackground(defaultPlanSelected);
  planName = null;
  money = null;
}

setDefaultPlanAndPrice();

let toggle = document.querySelector(".toggle__fill");
toggle.addEventListener("click", changePlanType);

function changePlanType() {
  toggleCount++;

  let freePlan = document.querySelectorAll(".free-plan");
  let year = document.getElementById("year-text");
  let month = document.getElementById("month-text");

  if (toggleCount % 2 === yearlyPlan) {
    showFreePlan(freePlan);
    displayAnnualFee();
    setTextBackground(month);
    resetTextBackground(year);
    setDefaultPlanAndPrice();
    if (plan !== null) resetPlanBackground(plan);
  } else {
    hideFreePlan(freePlan);
    displayMonthlyFee();
    setTextBackground(year);
    resetTextBackground(month);
    setDefaultPlanAndPrice();
    if (plan !== null) resetPlanBackground(plan);
  }
}

function showFreePlan(freePlan) {
  freePlan.forEach((plan) => {
    plan.style.display = "block";
  });
}

function hideFreePlan(freePlan) {
  freePlan.forEach((plan) => {
    plan.style.display = "none";
  });
}

function displayAnnualFee() {
  let yearlyPrices = ["+$90/yr", "+$120/yr", "+$150/yr"];

  let allMoneyClass = document.querySelectorAll(".money-text");

  allMoneyClass.forEach((money, index) => {
    money.textContent = yearlyPrices[index];
  });
}

function displayMonthlyFee() {
  let allMoneyClass = document.querySelectorAll(".money-text");

  let monthlyPrices = ["+$9/mo", "+$12/mo", "+$15/mo"];

  allMoneyClass.forEach((money, index) => {
    money.textContent = monthlyPrices[index];
  });
}

function setTextBackground(element) {
  element.style.color = "hsl(231, 11%, 63%)";
}

function resetTextBackground(element) {
  element.style.color = "black";
}

/* plan selection operations such as changing plans types*/

let currentPlan = defaultPlanSelected;
let previousPlan;

let planContainer = document.querySelector(".plan-container");
planContainer.addEventListener("click", selectPlan);

function selectPlan(clickEvent) {
  resetPlanBackground(defaultPlanSelected);
  let clicked = clickEvent.target;

  plan = clicked.closest(".plan");
  if (plan) {
    previousPlan = currentPlan;
    currentPlan = plan;

    resetPlanBackground(previousPlan);
    setPlanBackground(currentPlan);

    planName = plan.children[planTextIndex].children[planNameIndex].textContent;
    money = plan.children[planTextIndex].children[moneyIndex].textContent;
  } else {
    return;
  }
}

function setPlanBackground(element) {
  if (element !== null) {
    element.style.backgroundColor = "hsl(217, 100%, 97%)";
    element.style.border = "1px solid hsl(213, 96%, 18%)";
  }
}

function resetPlanBackground(element) {
  if (element !== null) {
    element.style.backgroundColor = "white";
    element.style.border = "1.5px solid hsl(229, 24%, 87%)";
  }
}

// add on page

function showAddOnPage(planType) {
  let heading = "Pick add-ons";
  let subHeading = "Add-ons help enhance your gaming experience";
  changeHeadingContent(heading, subHeading);

  let addOn = document.getElementById("add-ons-details");
  addOn.classList.remove("display-none");

  if (planType === "yearly") {
    showYearlyPrice();
  } else {
    showMonthlyPrice();
  }

  showBackButton();

  let secondNav = document.getElementById("second-nav");
  resetNavigationBackground(secondNav);

  let thirdNav = document.getElementById("third-nav");
  setNavigationBackground(thirdNav);
}

function showYearlyPrice() {
  let prices = document.querySelectorAll(".price");
  let yearlyPrices = ["+$10/yr", "+$20/yr", "+$20/yr"];

  prices.forEach((price, index) => {
    price.textContent = yearlyPrices[index];
  });
}

function showMonthlyPrice() {
  let prices = document.querySelectorAll(".price");
  let monthlyPrices = ["+$1/mo", "+$2/mo", "+$2/mo"];

  prices.forEach((price, index) => {
    price.textContent = monthlyPrices[index];
  });
}

// add ons selection

let addOnsContainer = document.querySelector(".add-ons-container");
addOnsContainer.addEventListener("click", clickedAddOn);

function clickedAddOn(clickEvent) {
  let clicked = clickEvent.target;
  let addOn = clicked.closest(".add-ons");
  let checkbox = addOn.firstElementChild.firstElementChild;

  checkbox.addEventListener("change", () => {
    if (checkbox.checked) {
      setAddOnBackground(addOn);
    } else {
      resetAddOnBackground(addOn);
    }
  });
}

function setAddOnBackground(addOn) {
  addOn.style.backgroundColor = "hsl(217, 100%, 97%)";
  addOn.style.border = "1px solid hsl(213, 96%, 18%)";
}

function resetAddOnBackground(addOn) {
  addOn.style.backgroundColor = "white";
  addOn.style.border = "1px solid hsl(229, 24%, 87%)";
}

let selectedAddOns = [];
let addOnObject = {};

function validateAddOns() {
  page++;

  selectedAddOns = [];
  let checkboxes = document.querySelectorAll(".check");

  checkboxes.forEach((checkbox, index) => {
    if (checkbox.checked) {
      let name = checkbox.nextElementSibling.firstElementChild.textContent;
      let price = checkbox.parentElement.nextElementSibling.textContent;

      addOnObject.name = name;
      addOnObject.price = price;
      selectedAddOns.push(addOnObject);

      addOnObject = {};
    }
  });

  hideAddOnPage();
  showSummaryPage();
}

function hideAddOnPage() {
  let addOnPage = document.getElementById("add-ons-details");
  addOnPage.classList.add("display-none");
}

// summary page

function showSummaryPage() {
  let heading = "Finishing up";
  let subHeading = "Double-check everything looks OK before confirming";
  changeHeadingContent(heading, subHeading);

  let summaryPage = document.getElementById("finishing-up-wrapper");
  summaryPage.classList.remove("display-none");

  showBackButton();

  let thirdNav = document.getElementById("third-nav");
  resetNavigationBackground(thirdNav);

  let fourthNav = document.getElementById("fourth-nav");
  setNavigationBackground(fourthNav);

  displayPlan();
  displayAddOns();
  calculateTotalPrice();
  changeNextButtonText();
}

function displayPlan() {
  let finalizedPlan = planName || defaultPlanName;
  let finalizedMoney = money || defaultMoney;

  let finalizedPlanName = document.getElementById("finalized-plan-name");
  finalizedPlanName.textContent = `${finalizedPlan} (${planType})`;

  let finalizedPlanCost = document.getElementById("finalized-plan-cost");
  finalizedPlanCost.textContent = finalizedMoney;
}

function displayAddOns() {
  let selectedAddOnsContainer = document.getElementById("selected-addons");

  if (selectedAddOns.length > 0) {
    document.getElementById("line-break").style.visibility = "visible";

    clearAddOnsContainer(selectedAddOnsContainer);

    selectedAddOns.forEach((addOn) => {
      let finalizedAddOn = document.createElement("div");
      finalizedAddOn.classList.add("finalized-add-on");

      let addOnName = document.createElement("div");
      let addOnPrice = document.createElement("div");

      addOnName.textContent = addOn.name;
      addOnName.classList.add("add-on-name");

      addOnPrice.textContent = addOn.price;
      addOnPrice.classList.add("add-on-price");

      finalizedAddOn.appendChild(addOnName);
      finalizedAddOn.appendChild(addOnPrice);

      selectedAddOnsContainer.appendChild(finalizedAddOn);
    });
  } else {
    document.getElementById("line-break").style.visibility = "hidden";
    clearAddOnsContainer(selectedAddOnsContainer);
  }
}

function clearAddOnsContainer(selectedAddOnsContainer) {
  let allDiv = selectedAddOnsContainer.querySelectorAll("div");
  allDiv.forEach((div) => div.remove());
}

function calculateTotalPrice() {
  let totalPrice = 0;
  let addOnsPrice = 0;
  let planMoney = money || defaultMoney;

  let moneyRegex = /(\d+)/;
  selectedAddOns.forEach((addOn) => {
    let price = addOn.price.match(moneyRegex);
    addOnsPrice += parseInt(price[1]);
  });

  planMoney = planMoney.match(moneyRegex);
  let planPrice = parseInt(planMoney[1]);

  totalPrice = planPrice + addOnsPrice;
  displayTotalPrice(totalPrice);
}

function displayTotalPrice(totalPrice) {
  let totalPriceElement = document.querySelector(".total-cost");
  let totalCostLabel = document.querySelector(".total-cost-label");

  if (planType === "yearly") {
    totalPriceElement.textContent = `$ ${totalPrice}/yr`;
    totalCostLabel.textContent = "Total(per year)";
  } else {
    totalPriceElement.textContent = `$ ${totalPrice}/mo`;
    totalCostLabel.textContent = "Total(per month)";
  }
}

function changeNextButtonText() {
  let nextButton = document.getElementById("next-button");
  nextButton.textContent = "Confirm";
}

// thank you page

function displayThankYouPage() {
  hideSummaryPage();
  hideFooterButtons();
  hideHeading();

  let thankYouPage = document.querySelector(".thank-you-wrapper");
  thankYouPage.classList.remove("display-none");
}

function hideSummaryPage() {
  let summaryPage = document.getElementById("finishing-up-wrapper");
  summaryPage.classList.add("display-none");
}

function hideFooterButtons() {
  let footerButton = document.getElementById("footer-buttons");
  footerButton.classList.add("display-none");
}

function hideHeading() {
  let heading = document.querySelector(".heading");
  heading.classList.add("display-none");
}

// frequently used methods

function setNavigationBackground(nav) {
  nav.style.color = "black";
  nav.style.border = "none";
  nav.style.backgroundColor = "hsl(206, 94%, 87%)";
}

function resetNavigationBackground(nav) {
  nav.style.color = "white";
  nav.style.border = "1px white solid";
  nav.style.backgroundColor = "transparent";
}

function defaultNavigationBackground() {
  let firstNav = document.getElementById("first-nav");
  setNavigationBackground(firstNav);
}

defaultNavigationBackground();

function changeHeadingContent(heading, subheading) {
  let topHeading = document.querySelector(".top-heading");

  let subHeading = document.querySelector(".subheading");

  topHeading.firstElementChild.innerHTML = heading;
  subHeading.textContent = subheading;
}

function showBackButton() {
  let backButton = document.getElementById("back-button");
  backButton.style.visibility = "visible";
}

function hideBackButton() {
  let backButton = document.getElementById("back-button");
  backButton.style.visibility = "hidden";
}

// backward actions performed on back button click

backButton.addEventListener("click", backwardActions);
function backwardActions() {
  switch (page) {
    case 2:
      hidePlanPage();
      showFirstPage();
      hideBackButton();
      page--;
      break;
    case 3:
      hideAddOnPage();
      showSecondPage();
      resetThirdNavigation();
      page--;
      break;
    case 4:
      hideSummaryPage();
      showAddOnPage();
      resetFourthNavigation();
      changeConfirmButtonText();
      page--;
      break;
  }
}

function showFirstPage() {
  let personalInfoPage = document.getElementById("personal-details");
  personalInfoPage.classList.remove("display-none");

  let heading = "Personal info";
  let subHeading = "Please provide your name,email address, and phone number.";
  changeHeadingContent(heading, subHeading);
  let firstNav = document.getElementById("first-nav");
  let secondNav = document.getElementById("second-nav");
  resetNavigationBackground(secondNav);
  setNavigationBackground(firstNav);
}

function resetThirdNavigation() {
  let thirdNav = document.getElementById("third-nav");
  resetNavigationBackground(thirdNav);
}

function resetFourthNavigation() {
  let fourthNav = document.getElementById("fourth-nav");
  resetNavigationBackground(fourthNav);
}

function changeConfirmButtonText() {
  let nextButton = document.getElementById("next-button");
  nextButton.textContent = "Next";
}

let changePlan = document.getElementById("change");
changePlan.addEventListener("click", navigateToPlanPage);

function navigateToPlanPage() {
  page = 2;
  hideSummaryPage();
  showSecondPage();
  resetFourthNavigation();
}
